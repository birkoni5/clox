﻿namespace Clox
{
    partial class uctrl_comm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uctrl_comm));
            this.label1 = new System.Windows.Forms.Label();
            this.btn_wifi = new System.Windows.Forms.Button();
            this.btn_uart = new System.Windows.Forms.Button();
            this.pnl_side = new System.Windows.Forms.Panel();
            this.cmb_ports = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.tb_ip = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(147, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(443, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Choose a way to communicate to the STM32 controller:";
            this.label1.Visible = false;
            // 
            // btn_wifi
            // 
            this.btn_wifi.FlatAppearance.BorderSize = 0;
            this.btn_wifi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_wifi.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_wifi.ForeColor = System.Drawing.Color.Maroon;
            this.btn_wifi.Image = ((System.Drawing.Image)(resources.GetObject("btn_wifi.Image")));
            this.btn_wifi.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_wifi.Location = new System.Drawing.Point(425, 100);
            this.btn_wifi.Name = "btn_wifi";
            this.btn_wifi.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btn_wifi.Size = new System.Drawing.Size(165, 60);
            this.btn_wifi.TabIndex = 4;
            this.btn_wifi.Text = "  WiFi";
            this.btn_wifi.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btn_wifi.UseVisualStyleBackColor = true;
            this.btn_wifi.Click += new System.EventHandler(this.btn_wifi_Click);
            // 
            // btn_uart
            // 
            this.btn_uart.FlatAppearance.BorderSize = 0;
            this.btn_uart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_uart.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_uart.ForeColor = System.Drawing.Color.Maroon;
            this.btn_uart.Image = ((System.Drawing.Image)(resources.GetObject("btn_uart.Image")));
            this.btn_uart.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_uart.Location = new System.Drawing.Point(425, 165);
            this.btn_uart.Name = "btn_uart";
            this.btn_uart.Size = new System.Drawing.Size(165, 60);
            this.btn_uart.TabIndex = 5;
            this.btn_uart.Text = "  UART";
            this.btn_uart.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btn_uart.UseVisualStyleBackColor = true;
            this.btn_uart.Click += new System.EventHandler(this.btn_uart_Click);
            // 
            // pnl_side
            // 
            this.pnl_side.BackColor = System.Drawing.Color.Maroon;
            this.pnl_side.Location = new System.Drawing.Point(590, 100);
            this.pnl_side.Name = "pnl_side";
            this.pnl_side.Size = new System.Drawing.Size(10, 60);
            this.pnl_side.TabIndex = 9;
            this.pnl_side.Visible = false;
            // 
            // cmb_ports
            // 
            this.cmb_ports.BackColor = System.Drawing.Color.Gainsboro;
            this.cmb_ports.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_ports.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmb_ports.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmb_ports.ForeColor = System.Drawing.Color.Maroon;
            this.cmb_ports.FormattingEnabled = true;
            this.cmb_ports.Location = new System.Drawing.Point(298, 182);
            this.cmb_ports.Name = "cmb_ports";
            this.cmb_ports.Size = new System.Drawing.Size(121, 29);
            this.cmb_ports.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(247, 185);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 21);
            this.label2.TabIndex = 12;
            this.label2.Text = "Port:";
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.Maroon;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.Location = new System.Drawing.Point(76, 100);
            this.button1.Name = "button1";
            this.button1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.button1.Size = new System.Drawing.Size(165, 60);
            this.button1.TabIndex = 13;
            this.button1.Text = "SSID and IP from the ESP";
            this.button1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tb_ip
            // 
            this.tb_ip.BackColor = System.Drawing.Color.LightGray;
            this.tb_ip.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_ip.ForeColor = System.Drawing.Color.Maroon;
            this.tb_ip.Location = new System.Drawing.Point(277, 117);
            this.tb_ip.Name = "tb_ip";
            this.tb_ip.Size = new System.Drawing.Size(142, 27);
            this.tb_ip.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(247, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 21);
            this.label3.TabIndex = 15;
            this.label3.Text = "IP:";
            // 
            // uctrl_comm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tb_ip);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmb_ports);
            this.Controls.Add(this.pnl_side);
            this.Controls.Add(this.btn_uart);
            this.Controls.Add(this.btn_wifi);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.Maroon;
            this.Name = "uctrl_comm";
            this.Size = new System.Drawing.Size(600, 400);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_wifi;
        private System.Windows.Forms.Button btn_uart;
        private System.Windows.Forms.Panel pnl_side;
        private System.Windows.Forms.ComboBox cmb_ports;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tb_ip;
        private System.Windows.Forms.Label label3;
    }
}
