﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace Clox
{
    public partial class uctrl_comm : UserControl
    {
        char conn= 'w';
        string[] ports;
        string SSID;
        string IP;
        public event EventHandler ButtonClick1;
        public event EventHandler ButtonClick2;
        public event EventHandler ButtonClick3;
        public char connection
        {
        get { return conn; }
        }
        public string port
        {
            get { return ports[cmb_ports.SelectedIndex]; }
        }
        public string SSID_
        {
            get { return SSID; }
            set { SSID=value; }
        }
        public string IP_
        {
            get { return IP; }
            set { IP = value; }
        }

        public uctrl_comm()
        {
            InitializeComponent();
            ports = SerialPort.GetPortNames();
            cmb_ports.DataSource = ports;
            pnl_side.Top = btn_wifi.Top;
        }

        private void btn_wifi_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tb_ip.Text))
            {
                if (!pnl_side.Visible)
                    pnl_side.Visible = true;
                pnl_side.Top = btn_wifi.Top;
                if (this.ButtonClick2 != null)
                    this.ButtonClick2(this, e);
                conn = 'w';
                IP = tb_ip.Text;
            }
            else
                MessageBox.Show("Textbox for IP address is empty!");
        }

        private void btn_uart_Click(object sender, EventArgs e)
        {
            if (!pnl_side.Visible)
                pnl_side.Visible = true;
            pnl_side.Top = btn_uart.Top;
            conn = 'u';
            if (cmb_ports.SelectedIndex == -1)
            {
                MessageBox.Show("Please connect USB-to-UART converter and select the port.");
                ports = SerialPort.GetPortNames();
                cmb_ports.DataSource = ports;
            }
            else if (this.ButtonClick1 != null)
                this.ButtonClick1(this, e);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.ButtonClick3 != null)
                this.ButtonClick3(this, e);
            if (!string.IsNullOrEmpty(IP))
                tb_ip.Text = IP;
        }
    }
}
