﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clox
{
    public partial class uctrl_mode : UserControl
    {
        char m = 's';
        public event EventHandler ButtonClick;
        public char mode
        {
            get { return m; }
        }
        public uctrl_mode()
        {
            InitializeComponent();
            pnl_side.Top = btn_stby.Top;
        }
        private void btn_stby_Click(object sender, EventArgs e)
        {
            m = 's';
            pnl_side.Top = btn_stby.Top;
            if (this.ButtonClick != null)
                this.ButtonClick(this, e);
        }
        private void btn_clock_Click(object sender, EventArgs e)
        {
            m = 'c';
            pnl_side.Top = btn_clock.Top;
            if (this.ButtonClick != null)
                this.ButtonClick(this, e);
        }

        private void btn_lamp_Click(object sender, EventArgs e)
        {
            m = 'l';
            pnl_side.Top = btn_lamp.Top;
            if (this.ButtonClick != null)
                this.ButtonClick(this, e);
        }

        private void pnl_side_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
