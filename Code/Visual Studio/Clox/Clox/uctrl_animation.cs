﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clox
{
    public partial class uctrl_animation : UserControl
    {
        char a = 'b';
        public event EventHandler ButtonClick;
        public uctrl_animation()
        {
            InitializeComponent();
            pnl_side.Top = btn_basic.Top;
        }

        public char animation
        {
            get { return a; }
        }
        public bool n0
        {
            get { return rbtn_0.Checked; }
        }
        public bool n4
        {
            get { return rbtn_4.Checked; }
        }
        public bool n12
        {
            get { return rbtn_12.Checked; }
        }
        private void btn_basic_Click(object sender, EventArgs e)
        {
            a = 'b';
            pnl_side.Top = btn_basic.Top;
        }

        private void btn_r1_Click(object sender, EventArgs e)
        {
            a = '1';
            pnl_side.Top = btn_r1.Top;
        }

        private void btn_r2_Click(object sender, EventArgs e)
        {
            a = '2';
            pnl_side.Top = btn_r2.Top;
        }

        private void btn_send_Click(object sender, EventArgs e)
        {
            if (this.ButtonClick != null)
                this.ButtonClick(this, e);
        }
    }
}
