﻿namespace Clox
{
    partial class uctrl_time
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uctrl_time));
            this.btn_send = new System.Windows.Forms.Button();
            this.pnl_side = new System.Windows.Forms.Panel();
            this.btn_ntp = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.nud_hour = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.nud_min = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.nud_sec = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.nud_hour)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_min)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_sec)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_send
            // 
            this.btn_send.FlatAppearance.BorderSize = 0;
            this.btn_send.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_send.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_send.ForeColor = System.Drawing.Color.Maroon;
            this.btn_send.Image = ((System.Drawing.Image)(resources.GetObject("btn_send.Image")));
            this.btn_send.Location = new System.Drawing.Point(530, 330);
            this.btn_send.Name = "btn_send";
            this.btn_send.Size = new System.Drawing.Size(60, 60);
            this.btn_send.TabIndex = 20;
            this.btn_send.UseVisualStyleBackColor = true;
            this.btn_send.Click += new System.EventHandler(this.btn_send_Click);
            // 
            // pnl_side
            // 
            this.pnl_side.BackColor = System.Drawing.Color.Maroon;
            this.pnl_side.Location = new System.Drawing.Point(590, 100);
            this.pnl_side.Name = "pnl_side";
            this.pnl_side.Size = new System.Drawing.Size(10, 60);
            this.pnl_side.TabIndex = 19;
            this.pnl_side.Visible = false;
            // 
            // btn_ntp
            // 
            this.btn_ntp.FlatAppearance.BorderSize = 0;
            this.btn_ntp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ntp.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ntp.ForeColor = System.Drawing.Color.Maroon;
            this.btn_ntp.Image = ((System.Drawing.Image)(resources.GetObject("btn_ntp.Image")));
            this.btn_ntp.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_ntp.Location = new System.Drawing.Point(425, 100);
            this.btn_ntp.Name = "btn_ntp";
            this.btn_ntp.Size = new System.Drawing.Size(165, 60);
            this.btn_ntp.TabIndex = 18;
            this.btn_ntp.Text = "  NTP time";
            this.btn_ntp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btn_ntp.UseVisualStyleBackColor = true;
            this.btn_ntp.Click += new System.EventHandler(this.btn_ntp_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(419, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(171, 21);
            this.label1.TabIndex = 16;
            this.label1.Text = "Set the time on Clox:";
            this.label1.Visible = false;
            // 
            // nud_hour
            // 
            this.nud_hour.BackColor = System.Drawing.Color.Gainsboro;
            this.nud_hour.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nud_hour.Font = new System.Drawing.Font("Century Gothic", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nud_hour.ForeColor = System.Drawing.Color.Maroon;
            this.nud_hour.Location = new System.Drawing.Point(20, 105);
            this.nud_hour.Maximum = new decimal(new int[] {
            23,
            0,
            0,
            0});
            this.nud_hour.Name = "nud_hour";
            this.nud_hour.Size = new System.Drawing.Size(60, 49);
            this.nud_hour.TabIndex = 21;
            this.nud_hour.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(80, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 44);
            this.label2.TabIndex = 22;
            this.label2.Text = "h:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(193, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 44);
            this.label3.TabIndex = 24;
            this.label3.Text = "min:";
            // 
            // nud_min
            // 
            this.nud_min.BackColor = System.Drawing.Color.Gainsboro;
            this.nud_min.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nud_min.Font = new System.Drawing.Font("Century Gothic", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nud_min.ForeColor = System.Drawing.Color.Maroon;
            this.nud_min.Location = new System.Drawing.Point(133, 105);
            this.nud_min.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.nud_min.Name = "nud_min";
            this.nud_min.Size = new System.Drawing.Size(60, 49);
            this.nud_min.TabIndex = 23;
            this.nud_min.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(347, 108);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 44);
            this.label4.TabIndex = 26;
            this.label4.Text = "s";
            // 
            // nud_sec
            // 
            this.nud_sec.BackColor = System.Drawing.Color.Gainsboro;
            this.nud_sec.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.nud_sec.Font = new System.Drawing.Font("Century Gothic", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nud_sec.ForeColor = System.Drawing.Color.Maroon;
            this.nud_sec.Location = new System.Drawing.Point(287, 105);
            this.nud_sec.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.nud_sec.Name = "nud_sec";
            this.nud_sec.Size = new System.Drawing.Size(60, 49);
            this.nud_sec.TabIndex = 25;
            this.nud_sec.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // uctrl_time
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.nud_sec);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.nud_min);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.nud_hour);
            this.Controls.Add(this.btn_send);
            this.Controls.Add(this.pnl_side);
            this.Controls.Add(this.btn_ntp);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.Maroon;
            this.Name = "uctrl_time";
            this.Size = new System.Drawing.Size(600, 400);
            ((System.ComponentModel.ISupportInitialize)(this.nud_hour)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_min)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_sec)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_send;
        private System.Windows.Forms.Panel pnl_side;
        private System.Windows.Forms.Button btn_ntp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nud_hour;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nud_min;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nud_sec;
    }
}
