﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Clox
{
    public partial class uctrl_color : UserControl
    {
        public event EventHandler ButtonClick;
        public uctrl_color()
        {
            InitializeComponent();
            pnl_side.Top = btn_hour.Top;
            pnl_hour.BackColor = Color.Red;
            pnl_min.BackColor = Color.Green;
            pnl_sec.BackColor = Color.Blue;
            pnl_back.BackColor = Color.Black;
            trb_brig.Value = 22;
        }
        public string color
        {
            get { return RGBConverter(Bright(pnl_hour.BackColor))+"."+ RGBConverter(Bright(pnl_min.BackColor)) + "."
                    + RGBConverter(Bright(pnl_sec.BackColor)) +"." + RGBConverter(Bright(pnl_back.BackColor)); }
        }
        public int brightness
        {
            get
            {
                return trb_brig.Value;
            }
        }
        private static String RGBConverter(Color c)
        {
            return c.R.ToString() + "." + c.G.ToString() + "." + c.B.ToString();
        }
        private Color Bright(Color c)
        {
            return Color.FromArgb(c.R*brightness / 100, c.G * brightness / 100, c.B * brightness / 100);
        }

        private void btn_hour_Click(object sender, EventArgs e)
        {
            if (!pnl_side.Visible)
                pnl_side.Visible = true;
            pnl_side.Top = btn_hour.Top;
            colorDialog1.ShowDialog();
            pnl_hour.BackColor = colorDialog1.Color;
        }

        private void btn_min_Click(object sender, EventArgs e)
        {
            if (!pnl_side.Visible)
                pnl_side.Visible = true;
            pnl_side.Top = btn_min.Top;
            colorDialog1.ShowDialog();
            pnl_min.BackColor = colorDialog1.Color;
        }

        private void btn_sec_Click(object sender, EventArgs e)
        {
            if (!pnl_side.Visible)
                pnl_side.Visible = true;
            pnl_side.Top = btn_sec.Top;
            colorDialog1.ShowDialog();
            pnl_sec.BackColor = colorDialog1.Color;
        }

        private void btn_back_Click(object sender, EventArgs e)
        {
            if (!pnl_side.Visible)
                pnl_side.Visible = true;
            pnl_side.Top = btn_back.Top;
            colorDialog1.ShowDialog();
            pnl_back.BackColor = colorDialog1.Color;
        }

        private void btn_def_Click(object sender, EventArgs e)
        {
            pnl_hour.BackColor = Color.Red;
            pnl_min.BackColor = Color.Green;
            pnl_sec.BackColor = Color.Blue;
            pnl_back.BackColor = Color.Black;
            trb_brig.Value = 22;
        }

        private void btn_send_Click(object sender, EventArgs e)
        {
            if (this.ButtonClick != null)
                this.ButtonClick(this, e);
        }
    }
}
