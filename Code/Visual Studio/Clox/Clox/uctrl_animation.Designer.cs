﻿namespace Clox
{
    partial class uctrl_animation
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uctrl_animation));
            this.btn_r2 = new System.Windows.Forms.Button();
            this.pnl_side = new System.Windows.Forms.Panel();
            this.btn_r1 = new System.Windows.Forms.Button();
            this.btn_basic = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_send = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.rbtn_12 = new System.Windows.Forms.RadioButton();
            this.rbtn_4 = new System.Windows.Forms.RadioButton();
            this.rbtn_0 = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // btn_r2
            // 
            this.btn_r2.FlatAppearance.BorderSize = 0;
            this.btn_r2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_r2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_r2.ForeColor = System.Drawing.Color.Maroon;
            this.btn_r2.Image = ((System.Drawing.Image)(resources.GetObject("btn_r2.Image")));
            this.btn_r2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_r2.Location = new System.Drawing.Point(425, 190);
            this.btn_r2.Name = "btn_r2";
            this.btn_r2.Size = new System.Drawing.Size(165, 60);
            this.btn_r2.TabIndex = 20;
            this.btn_r2.Text = "  Rotating 2";
            this.btn_r2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btn_r2.UseVisualStyleBackColor = true;
            this.btn_r2.Click += new System.EventHandler(this.btn_r2_Click);
            // 
            // pnl_side
            // 
            this.pnl_side.BackColor = System.Drawing.Color.Maroon;
            this.pnl_side.Location = new System.Drawing.Point(590, 60);
            this.pnl_side.Name = "pnl_side";
            this.pnl_side.Size = new System.Drawing.Size(10, 60);
            this.pnl_side.TabIndex = 19;
            // 
            // btn_r1
            // 
            this.btn_r1.FlatAppearance.BorderSize = 0;
            this.btn_r1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_r1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_r1.ForeColor = System.Drawing.Color.Maroon;
            this.btn_r1.Image = ((System.Drawing.Image)(resources.GetObject("btn_r1.Image")));
            this.btn_r1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_r1.Location = new System.Drawing.Point(425, 125);
            this.btn_r1.Name = "btn_r1";
            this.btn_r1.Size = new System.Drawing.Size(165, 60);
            this.btn_r1.TabIndex = 18;
            this.btn_r1.Text = "  Rotating 1";
            this.btn_r1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btn_r1.UseVisualStyleBackColor = true;
            this.btn_r1.Click += new System.EventHandler(this.btn_r1_Click);
            // 
            // btn_basic
            // 
            this.btn_basic.FlatAppearance.BorderSize = 0;
            this.btn_basic.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_basic.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_basic.ForeColor = System.Drawing.Color.Maroon;
            this.btn_basic.Image = ((System.Drawing.Image)(resources.GetObject("btn_basic.Image")));
            this.btn_basic.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_basic.Location = new System.Drawing.Point(425, 60);
            this.btn_basic.Name = "btn_basic";
            this.btn_basic.Size = new System.Drawing.Size(165, 60);
            this.btn_basic.TabIndex = 17;
            this.btn_basic.Text = "  Basic";
            this.btn_basic.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btn_basic.UseVisualStyleBackColor = true;
            this.btn_basic.Click += new System.EventHandler(this.btn_basic_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(329, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(261, 21);
            this.label1.TabIndex = 16;
            this.label1.Text = "Set the clock animation on Clox:";
            this.label1.Visible = false;
            // 
            // btn_send
            // 
            this.btn_send.FlatAppearance.BorderSize = 0;
            this.btn_send.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_send.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_send.ForeColor = System.Drawing.Color.Maroon;
            this.btn_send.Image = ((System.Drawing.Image)(resources.GetObject("btn_send.Image")));
            this.btn_send.Location = new System.Drawing.Point(530, 330);
            this.btn_send.Name = "btn_send";
            this.btn_send.Size = new System.Drawing.Size(60, 60);
            this.btn_send.TabIndex = 21;
            this.btn_send.UseVisualStyleBackColor = true;
            this.btn_send.Click += new System.EventHandler(this.btn_send_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(130, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(172, 21);
            this.label2.TabIndex = 24;
            this.label2.Text = "Show clock numbers:";
            // 
            // rbtn_12
            // 
            this.rbtn_12.AutoSize = true;
            this.rbtn_12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtn_12.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_12.Location = new System.Drawing.Point(308, 142);
            this.rbtn_12.Name = "rbtn_12";
            this.rbtn_12.Size = new System.Drawing.Size(45, 25);
            this.rbtn_12.TabIndex = 23;
            this.rbtn_12.Text = "12";
            this.rbtn_12.UseVisualStyleBackColor = true;
            // 
            // rbtn_4
            // 
            this.rbtn_4.AutoSize = true;
            this.rbtn_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtn_4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_4.Location = new System.Drawing.Point(308, 111);
            this.rbtn_4.Name = "rbtn_4";
            this.rbtn_4.Size = new System.Drawing.Size(36, 25);
            this.rbtn_4.TabIndex = 22;
            this.rbtn_4.Text = "4";
            this.rbtn_4.UseVisualStyleBackColor = true;
            // 
            // rbtn_0
            // 
            this.rbtn_0.AutoSize = true;
            this.rbtn_0.Checked = true;
            this.rbtn_0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtn_0.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_0.Location = new System.Drawing.Point(308, 80);
            this.rbtn_0.Name = "rbtn_0";
            this.rbtn_0.Size = new System.Drawing.Size(36, 25);
            this.rbtn_0.TabIndex = 25;
            this.rbtn_0.TabStop = true;
            this.rbtn_0.Text = "0";
            this.rbtn_0.UseVisualStyleBackColor = true;
            // 
            // uctrl_animation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.Controls.Add(this.rbtn_0);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.rbtn_12);
            this.Controls.Add(this.rbtn_4);
            this.Controls.Add(this.btn_send);
            this.Controls.Add(this.btn_r2);
            this.Controls.Add(this.pnl_side);
            this.Controls.Add(this.btn_r1);
            this.Controls.Add(this.btn_basic);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.Maroon;
            this.Name = "uctrl_animation";
            this.Size = new System.Drawing.Size(600, 400);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_r2;
        private System.Windows.Forms.Panel pnl_side;
        private System.Windows.Forms.Button btn_r1;
        private System.Windows.Forms.Button btn_basic;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_send;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rbtn_12;
        private System.Windows.Forms.RadioButton rbtn_4;
        private System.Windows.Forms.RadioButton rbtn_0;
    }
}
