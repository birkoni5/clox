﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Net.Http;

namespace Clox
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            pnl_side.Top = btn_comm.Top;
            uctrl_comm1.BringToFront();
            uctrl_comm1.ButtonClick1 += new EventHandler(uctrl_comm_u_Click);
            uctrl_comm1.ButtonClick2 += new EventHandler(uctrl_comm_w_Click);
            uctrl_comm1.ButtonClick3 += new EventHandler(uctrl_comm_i_Click);
            uctrl_mode1.ButtonClick += new EventHandler(uctrl_mode_Click);
            uctrl_color1.ButtonClick += new EventHandler(uctrl_color_Click);
            uctrl_time1.ButtonClick += new EventHandler(uctrl_time_Click);
            uctrl_anim1.ButtonClick += new EventHandler(uctrl_anim_Click);
        }
        public char connection
        {
            get { return uctrl_comm1.connection; }
        }
        protected void uctrl_comm_u_Click(object sender, EventArgs e)
        {
            if (!serialPort1.IsOpen)
            {
                serialPort1.PortName = uctrl_comm1.port;
                try{ serialPort1.Open(); }
                catch ( Exception ex){ MessageBox.Show(ex.Message); }
                if (serialPort1.IsOpen)
                    MessageBox.Show("Successfully connected.");
            }
        }
        protected void uctrl_comm_w_Click(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
                serialPort1.Close();
        }
        protected void uctrl_comm_i_Click(object sender, EventArgs e)
        {
            if (!serialPort1.IsOpen)
                MessageBox.Show("USB is not connected.");
            else
            {
                serialPort1.Write("Clox.w!");
                serialPort1.ReadTimeout = 500;
                try {
                    uctrl_comm1.SSID_ = serialPort1.ReadTo(".");
                    if (string.IsNullOrEmpty(uctrl_comm1.SSID_))
                        MessageBox.Show("ESP is not connected to a WiFi network.");
                    else if (uctrl_comm1.SSID_ == "\n" || uctrl_comm1.SSID_ == "\r")
                        uctrl_comm1.SSID_ = "";
                    else
                    {
                        uctrl_comm1.IP_ = serialPort1.ReadTo("\r");
                        MessageBox.Show("SSID: " + uctrl_comm1.SSID_ + "\nIP address: " + uctrl_comm1.IP_);
                    }
                }
                catch (Exception ex) { MessageBox.Show(ex.Message+" Check the USB connection."); }
            }
        }
        protected void uctrl_mode_Click(object sender, EventArgs e)
        {
            if (connection == 'u')
            {
                if (!serialPort1.IsOpen)
                {
                    MessageBox.Show("USB is not connected.");
                    pnl_side.Top = btn_comm.Top;
                    uctrl_comm1.BringToFront();
                }
                else
                    serialPort1.Write("Clox.m" + uctrl_mode1.mode + '!');
            }
            else if (connection == 'w')
                request("m" + uctrl_mode1.mode + "!");
        }
        protected void uctrl_color_Click(object sender, EventArgs e)
        {
            if (connection == 'u')
            {
                if (!serialPort1.IsOpen)
                {
                    MessageBox.Show("USB is not connected.");
                    pnl_side.Top = btn_comm.Top;
                    uctrl_comm1.BringToFront();
                }
                else
                    serialPort1.Write("Clox.c" + uctrl_color1.color + "!" + "Clox.b" + uctrl_color1.brightness * 7 / 100 + "!"); //7 - max value that can be set on TM1637
            }
            else if (connection == 'w')
            {
                request("c" + uctrl_color1.color + "!");
                request("b" + uctrl_color1.brightness * 7 / 100 + "!");
            }
         }
        protected void uctrl_time_Click(object sender, EventArgs e)
        {
            if (connection == 'u')
            {
                if (!serialPort1.IsOpen)
                {
                    MessageBox.Show("USB is not connected.");
                    pnl_side.Top = btn_comm.Top;
                    uctrl_comm1.BringToFront();
                }
                else
                    serialPort1.Write("Clox.t" + uctrl_time1.time + "!");
            }
            else if (connection == 'w')
                request("t" + uctrl_time1.time + "!");
        }
        protected void uctrl_anim_Click(object sender, EventArgs e)
        {
            if (connection == 'u')
            {
                if (!serialPort1.IsOpen)
                {
                    MessageBox.Show("USB is not connected.");
                    pnl_side.Top = btn_comm.Top;
                    uctrl_comm1.BringToFront();
                }
                else
                {
                    serialPort1.Write("Clox.a" + uctrl_anim1.animation + "!");
                    if (uctrl_anim1.n0)
                        serialPort1.Write("Clox.az!");
                    else if (uctrl_anim1.n4)
                        serialPort1.Write("Clox.af!");
                    else if (uctrl_anim1.n12)
                        serialPort1.Write("Clox.at!");
                }
            }
            else if (connection == 'w')
            {
                request("a" + uctrl_anim1.animation + "!");
                if (uctrl_anim1.n0)
                    request("az!");
                else if (uctrl_anim1.n4)
                    request("af!");
                else if (uctrl_anim1.n12)
                    request("at!");
            }
        }
        private void request(string uri)
        {
            try
            {
                if(string.IsNullOrEmpty(uctrl_comm1.IP_))
                    MessageBox.Show("There is no IP address.");
                else
                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://" + uctrl_comm1.IP_ + "/" + uri);
                    request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                    request.Timeout = 1000;
                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    using (Stream stream = response.GetResponseStream())
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        Console.WriteLine(reader.ReadToEnd());
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void btn_comm_Click(object sender, EventArgs e)
        {
            pnl_side.Top = btn_comm.Top;
            uctrl_comm1.BringToFront();
        }

        private void btn_mode_Click(object sender, EventArgs e)
        {
            pnl_side.Top = btn_mode.Top;
            uctrl_mode1.BringToFront();
        }

        private void btn_anim_Click(object sender, EventArgs e)
        {
            pnl_side.Top = btn_anim.Top;
            uctrl_anim1.BringToFront();
        }

        private void btn_color_Click(object sender, EventArgs e)
        {
            pnl_side.Top = btn_color.Top;
            uctrl_color1.BringToFront();
        }

        private void btn_time_Click(object sender, EventArgs e)
        {
            pnl_side.Top = btn_time.Top;
            uctrl_time1.BringToFront();
        }

        private void btn_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        //make form draggable
        private bool dragging = false;
        private Point offset;
        private Point start_point;
        private void panel3_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                dragging = true;
                start_point = new Point(e.X, e.Y);
            }
        }

        private void panel3_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }

        private void panel3_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point p = PointToScreen(e.Location);
                Location = new Point(p.X - this.start_point.X, p.Y - this.start_point.Y);
            }
        }

        private void btn_mini_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
    }
}
