﻿namespace Clox
{
    partial class uctrl_color
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uctrl_color));
            this.btn_sec = new System.Windows.Forms.Button();
            this.pnl_side = new System.Windows.Forms.Panel();
            this.btn_min = new System.Windows.Forms.Button();
            this.btn_hour = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.trb_brig = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_back = new System.Windows.Forms.Button();
            this.btn_def = new System.Windows.Forms.Button();
            this.pnl_hour = new System.Windows.Forms.Panel();
            this.pnl_min = new System.Windows.Forms.Panel();
            this.pnl_sec = new System.Windows.Forms.Panel();
            this.pnl_back = new System.Windows.Forms.Panel();
            this.btn_send = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.trb_brig)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_sec
            // 
            this.btn_sec.FlatAppearance.BorderSize = 0;
            this.btn_sec.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_sec.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_sec.ForeColor = System.Drawing.Color.Maroon;
            this.btn_sec.Location = new System.Drawing.Point(425, 190);
            this.btn_sec.Name = "btn_sec";
            this.btn_sec.Size = new System.Drawing.Size(165, 60);
            this.btn_sec.TabIndex = 20;
            this.btn_sec.Text = "Second hand";
            this.btn_sec.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btn_sec.UseVisualStyleBackColor = true;
            this.btn_sec.Click += new System.EventHandler(this.btn_sec_Click);
            // 
            // pnl_side
            // 
            this.pnl_side.BackColor = System.Drawing.Color.Maroon;
            this.pnl_side.Location = new System.Drawing.Point(590, 60);
            this.pnl_side.Name = "pnl_side";
            this.pnl_side.Size = new System.Drawing.Size(10, 60);
            this.pnl_side.TabIndex = 19;
            this.pnl_side.Visible = false;
            // 
            // btn_min
            // 
            this.btn_min.FlatAppearance.BorderSize = 0;
            this.btn_min.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_min.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_min.ForeColor = System.Drawing.Color.Maroon;
            this.btn_min.Location = new System.Drawing.Point(425, 125);
            this.btn_min.Name = "btn_min";
            this.btn_min.Size = new System.Drawing.Size(165, 60);
            this.btn_min.TabIndex = 18;
            this.btn_min.Text = "Minute hand";
            this.btn_min.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btn_min.UseVisualStyleBackColor = true;
            this.btn_min.Click += new System.EventHandler(this.btn_min_Click);
            // 
            // btn_hour
            // 
            this.btn_hour.FlatAppearance.BorderSize = 0;
            this.btn_hour.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_hour.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_hour.ForeColor = System.Drawing.Color.Maroon;
            this.btn_hour.Location = new System.Drawing.Point(425, 60);
            this.btn_hour.Name = "btn_hour";
            this.btn_hour.Size = new System.Drawing.Size(165, 60);
            this.btn_hour.TabIndex = 17;
            this.btn_hour.Text = "Hour hand";
            this.btn_hour.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btn_hour.UseVisualStyleBackColor = true;
            this.btn_hour.Click += new System.EventHandler(this.btn_hour_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(278, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(312, 21);
            this.label1.TabIndex = 16;
            this.label1.Text = "Select colors and adjust the brightness:";
            this.label1.Visible = false;
            // 
            // trb_brig
            // 
            this.trb_brig.BackColor = System.Drawing.Color.LightGray;
            this.trb_brig.Location = new System.Drawing.Point(300, 60);
            this.trb_brig.Maximum = 100;
            this.trb_brig.Name = "trb_brig";
            this.trb_brig.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trb_brig.Size = new System.Drawing.Size(45, 190);
            this.trb_brig.TabIndex = 21;
            this.trb_brig.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trb_brig.Value = 22;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(203, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 21);
            this.label2.TabIndex = 22;
            this.label2.Text = "Brightness:";
            // 
            // btn_back
            // 
            this.btn_back.FlatAppearance.BorderSize = 0;
            this.btn_back.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_back.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_back.ForeColor = System.Drawing.Color.Maroon;
            this.btn_back.Location = new System.Drawing.Point(425, 255);
            this.btn_back.Name = "btn_back";
            this.btn_back.Size = new System.Drawing.Size(165, 60);
            this.btn_back.TabIndex = 23;
            this.btn_back.Text = "Background";
            this.btn_back.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btn_back.UseVisualStyleBackColor = true;
            this.btn_back.Click += new System.EventHandler(this.btn_back_Click);
            // 
            // btn_def
            // 
            this.btn_def.FlatAppearance.BorderSize = 0;
            this.btn_def.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_def.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_def.ForeColor = System.Drawing.Color.Maroon;
            this.btn_def.Image = ((System.Drawing.Image)(resources.GetObject("btn_def.Image")));
            this.btn_def.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_def.Location = new System.Drawing.Point(200, 255);
            this.btn_def.Name = "btn_def";
            this.btn_def.Size = new System.Drawing.Size(165, 60);
            this.btn_def.TabIndex = 24;
            this.btn_def.Text = "  Default";
            this.btn_def.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btn_def.UseVisualStyleBackColor = true;
            this.btn_def.Click += new System.EventHandler(this.btn_def_Click);
            // 
            // pnl_hour
            // 
            this.pnl_hour.BackColor = System.Drawing.Color.Red;
            this.pnl_hour.Location = new System.Drawing.Point(390, 75);
            this.pnl_hour.Name = "pnl_hour";
            this.pnl_hour.Size = new System.Drawing.Size(30, 30);
            this.pnl_hour.TabIndex = 25;
            // 
            // pnl_min
            // 
            this.pnl_min.BackColor = System.Drawing.Color.Green;
            this.pnl_min.ForeColor = System.Drawing.Color.Green;
            this.pnl_min.Location = new System.Drawing.Point(390, 140);
            this.pnl_min.Name = "pnl_min";
            this.pnl_min.Size = new System.Drawing.Size(30, 30);
            this.pnl_min.TabIndex = 26;
            // 
            // pnl_sec
            // 
            this.pnl_sec.BackColor = System.Drawing.Color.Blue;
            this.pnl_sec.ForeColor = System.Drawing.Color.Green;
            this.pnl_sec.Location = new System.Drawing.Point(390, 205);
            this.pnl_sec.Name = "pnl_sec";
            this.pnl_sec.Size = new System.Drawing.Size(30, 30);
            this.pnl_sec.TabIndex = 27;
            // 
            // pnl_back
            // 
            this.pnl_back.BackColor = System.Drawing.Color.Black;
            this.pnl_back.ForeColor = System.Drawing.Color.Green;
            this.pnl_back.Location = new System.Drawing.Point(390, 270);
            this.pnl_back.Name = "pnl_back";
            this.pnl_back.Size = new System.Drawing.Size(30, 30);
            this.pnl_back.TabIndex = 27;
            // 
            // btn_send
            // 
            this.btn_send.FlatAppearance.BorderSize = 0;
            this.btn_send.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_send.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_send.ForeColor = System.Drawing.Color.Maroon;
            this.btn_send.Image = ((System.Drawing.Image)(resources.GetObject("btn_send.Image")));
            this.btn_send.Location = new System.Drawing.Point(530, 330);
            this.btn_send.Name = "btn_send";
            this.btn_send.Size = new System.Drawing.Size(60, 60);
            this.btn_send.TabIndex = 28;
            this.btn_send.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btn_send.UseVisualStyleBackColor = true;
            this.btn_send.Click += new System.EventHandler(this.btn_send_Click);
            // 
            // uctrl_color
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.Controls.Add(this.btn_send);
            this.Controls.Add(this.pnl_back);
            this.Controls.Add(this.pnl_sec);
            this.Controls.Add(this.pnl_min);
            this.Controls.Add(this.pnl_hour);
            this.Controls.Add(this.btn_def);
            this.Controls.Add(this.btn_back);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.trb_brig);
            this.Controls.Add(this.btn_sec);
            this.Controls.Add(this.pnl_side);
            this.Controls.Add(this.btn_min);
            this.Controls.Add(this.btn_hour);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.Maroon;
            this.Name = "uctrl_color";
            this.Size = new System.Drawing.Size(600, 400);
            ((System.ComponentModel.ISupportInitialize)(this.trb_brig)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_sec;
        private System.Windows.Forms.Panel pnl_side;
        private System.Windows.Forms.Button btn_min;
        private System.Windows.Forms.Button btn_hour;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.TrackBar trb_brig;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_back;
        private System.Windows.Forms.Button btn_def;
        private System.Windows.Forms.Panel pnl_hour;
        private System.Windows.Forms.Panel pnl_min;
        private System.Windows.Forms.Panel pnl_sec;
        private System.Windows.Forms.Panel pnl_back;
        private System.Windows.Forms.Button btn_send;
    }
}
