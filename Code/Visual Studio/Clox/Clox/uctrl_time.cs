﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace Clox
{
    public partial class uctrl_time : UserControl
    {
        public event EventHandler ButtonClick;
        private int time_zone = 2;
        public uctrl_time()
        {
            InitializeComponent();
        }
        public string time
        {
            get { return nud_hour.Value.ToString() + "." + nud_min.Value.ToString() + "." + nud_sec.Value.ToString(); }
        }
        public static DateTime GetNetworkTime()
        {
            try
            {
                const string ntpServer = "pool.ntp.org";
                var ntpData = new byte[48];
                ntpData[0] = 0x1B; //LeapIndicator = 0 (no warning), VersionNum = 3 (IPv4 only), Mode = 3 (Client Mode)

                var addresses = Dns.GetHostEntry(ntpServer).AddressList;
                var ipEndPoint = new IPEndPoint(addresses[0], 123);
                var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

                socket.Connect(ipEndPoint);
                socket.Send(ntpData);
                socket.Receive(ntpData);
                socket.Close();

                ulong intPart = (ulong)ntpData[40] << 24 | (ulong)ntpData[41] << 16 | (ulong)ntpData[42] << 8 | (ulong)ntpData[43];
                ulong fractPart = (ulong)ntpData[44] << 24 | (ulong)ntpData[45] << 16 | (ulong)ntpData[46] << 8 | (ulong)ntpData[47];

                var milliseconds = (intPart * 1000) + ((fractPart * 1000) / 0x100000000L);
                var networkDateTime = (new DateTime(1900, 1, 1)).AddMilliseconds((long)milliseconds);

                return networkDateTime;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); return new DateTime(1900, 1, 1); }
}

        private void btn_ntp_Click(object sender, EventArgs e)
        {
            if (!pnl_side.Visible)
                pnl_side.Visible = true;
            pnl_side.Top = btn_ntp.Top;
            DateTime time = GetNetworkTime();
            nud_hour.Value = time.Hour+ time_zone;
            nud_min.Value = time.Minute;
            nud_sec.Value = time.Second;
        }

        private void btn_send_Click(object sender, EventArgs e)
        {
            if (this.ButtonClick != null)
                this.ButtonClick(this, e);
        }
    }
}
