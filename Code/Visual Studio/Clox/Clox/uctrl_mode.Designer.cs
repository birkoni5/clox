﻿namespace Clox
{
    partial class uctrl_mode
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(uctrl_mode));
            this.label1 = new System.Windows.Forms.Label();
            this.pnl_side = new System.Windows.Forms.Panel();
            this.btn_clock = new System.Windows.Forms.Button();
            this.btn_stby = new System.Windows.Forms.Button();
            this.btn_lamp = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(408, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(182, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "Set the mode on Clox:";
            this.label1.Visible = false;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // pnl_side
            // 
            this.pnl_side.BackColor = System.Drawing.Color.Maroon;
            this.pnl_side.Location = new System.Drawing.Point(590, 60);
            this.pnl_side.Name = "pnl_side";
            this.pnl_side.Size = new System.Drawing.Size(10, 60);
            this.pnl_side.TabIndex = 13;
            this.pnl_side.Paint += new System.Windows.Forms.PaintEventHandler(this.pnl_side_Paint);
            // 
            // btn_clock
            // 
            this.btn_clock.FlatAppearance.BorderSize = 0;
            this.btn_clock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_clock.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_clock.ForeColor = System.Drawing.Color.Maroon;
            this.btn_clock.Image = ((System.Drawing.Image)(resources.GetObject("btn_clock.Image")));
            this.btn_clock.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_clock.Location = new System.Drawing.Point(425, 125);
            this.btn_clock.Name = "btn_clock";
            this.btn_clock.Size = new System.Drawing.Size(165, 60);
            this.btn_clock.TabIndex = 12;
            this.btn_clock.Text = "  Clock";
            this.btn_clock.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btn_clock.UseVisualStyleBackColor = true;
            this.btn_clock.Click += new System.EventHandler(this.btn_clock_Click);
            // 
            // btn_stby
            // 
            this.btn_stby.FlatAppearance.BorderSize = 0;
            this.btn_stby.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_stby.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_stby.ForeColor = System.Drawing.Color.Maroon;
            this.btn_stby.Image = ((System.Drawing.Image)(resources.GetObject("btn_stby.Image")));
            this.btn_stby.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_stby.Location = new System.Drawing.Point(425, 60);
            this.btn_stby.Name = "btn_stby";
            this.btn_stby.Size = new System.Drawing.Size(165, 60);
            this.btn_stby.TabIndex = 11;
            this.btn_stby.Text = "  Stand by";
            this.btn_stby.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btn_stby.UseVisualStyleBackColor = true;
            this.btn_stby.Click += new System.EventHandler(this.btn_stby_Click);
            // 
            // btn_lamp
            // 
            this.btn_lamp.FlatAppearance.BorderSize = 0;
            this.btn_lamp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_lamp.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_lamp.ForeColor = System.Drawing.Color.Maroon;
            this.btn_lamp.Image = ((System.Drawing.Image)(resources.GetObject("btn_lamp.Image")));
            this.btn_lamp.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_lamp.Location = new System.Drawing.Point(425, 190);
            this.btn_lamp.Name = "btn_lamp";
            this.btn_lamp.Size = new System.Drawing.Size(165, 60);
            this.btn_lamp.TabIndex = 15;
            this.btn_lamp.Text = "  Lamp";
            this.btn_lamp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btn_lamp.UseVisualStyleBackColor = true;
            this.btn_lamp.Click += new System.EventHandler(this.btn_lamp_Click);
            // 
            // uctrl_mode
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.Controls.Add(this.btn_lamp);
            this.Controls.Add(this.pnl_side);
            this.Controls.Add(this.btn_clock);
            this.Controls.Add(this.btn_stby);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.Maroon;
            this.Name = "uctrl_mode";
            this.Size = new System.Drawing.Size(600, 400);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnl_side;
        private System.Windows.Forms.Button btn_clock;
        private System.Windows.Forms.Button btn_stby;
        private System.Windows.Forms.Button btn_lamp;
    }
}
