﻿namespace Clox
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnl_side = new System.Windows.Forms.Panel();
            this.btn_time = new System.Windows.Forms.Button();
            this.btn_color = new System.Windows.Forms.Button();
            this.btn_anim = new System.Windows.Forms.Button();
            this.btn_mode = new System.Windows.Forms.Button();
            this.btn_comm = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.btn_exit = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btn_mini = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.uctrl_anim1 = new Clox.uctrl_animation();
            this.uctrl_time1 = new Clox.uctrl_time();
            this.uctrl_comm1 = new Clox.uctrl_comm();
            this.uctrl_color1 = new Clox.uctrl_color();
            this.uctrl_mode1 = new Clox.uctrl_mode();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DimGray;
            this.panel1.Controls.Add(this.pnl_side);
            this.panel1.Controls.Add(this.btn_time);
            this.panel1.Controls.Add(this.btn_color);
            this.panel1.Controls.Add(this.btn_anim);
            this.panel1.Controls.Add(this.btn_mode);
            this.panel1.Controls.Add(this.btn_comm);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 500);
            this.panel1.TabIndex = 0;
            // 
            // pnl_side
            // 
            this.pnl_side.BackColor = System.Drawing.Color.Maroon;
            this.pnl_side.Location = new System.Drawing.Point(0, 135);
            this.pnl_side.Name = "pnl_side";
            this.pnl_side.Size = new System.Drawing.Size(10, 60);
            this.pnl_side.TabIndex = 8;
            // 
            // btn_time
            // 
            this.btn_time.FlatAppearance.BorderSize = 0;
            this.btn_time.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_time.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_time.ForeColor = System.Drawing.Color.Gainsboro;
            this.btn_time.Image = ((System.Drawing.Image)(resources.GetObject("btn_time.Image")));
            this.btn_time.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_time.Location = new System.Drawing.Point(10, 395);
            this.btn_time.Name = "btn_time";
            this.btn_time.Size = new System.Drawing.Size(190, 60);
            this.btn_time.TabIndex = 7;
            this.btn_time.Text = "  Time";
            this.btn_time.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btn_time.UseVisualStyleBackColor = true;
            this.btn_time.Click += new System.EventHandler(this.btn_time_Click);
            // 
            // btn_color
            // 
            this.btn_color.FlatAppearance.BorderSize = 0;
            this.btn_color.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_color.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_color.ForeColor = System.Drawing.Color.Gainsboro;
            this.btn_color.Image = ((System.Drawing.Image)(resources.GetObject("btn_color.Image")));
            this.btn_color.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_color.Location = new System.Drawing.Point(10, 330);
            this.btn_color.Name = "btn_color";
            this.btn_color.Size = new System.Drawing.Size(190, 60);
            this.btn_color.TabIndex = 6;
            this.btn_color.Text = "  Color";
            this.btn_color.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btn_color.UseVisualStyleBackColor = true;
            this.btn_color.Click += new System.EventHandler(this.btn_color_Click);
            // 
            // btn_anim
            // 
            this.btn_anim.FlatAppearance.BorderSize = 0;
            this.btn_anim.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_anim.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_anim.ForeColor = System.Drawing.Color.Gainsboro;
            this.btn_anim.Image = ((System.Drawing.Image)(resources.GetObject("btn_anim.Image")));
            this.btn_anim.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_anim.Location = new System.Drawing.Point(10, 265);
            this.btn_anim.Name = "btn_anim";
            this.btn_anim.Size = new System.Drawing.Size(190, 60);
            this.btn_anim.TabIndex = 5;
            this.btn_anim.Text = "  Animation";
            this.btn_anim.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btn_anim.UseVisualStyleBackColor = true;
            this.btn_anim.Click += new System.EventHandler(this.btn_anim_Click);
            // 
            // btn_mode
            // 
            this.btn_mode.FlatAppearance.BorderSize = 0;
            this.btn_mode.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_mode.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_mode.ForeColor = System.Drawing.Color.Gainsboro;
            this.btn_mode.Image = ((System.Drawing.Image)(resources.GetObject("btn_mode.Image")));
            this.btn_mode.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_mode.Location = new System.Drawing.Point(10, 200);
            this.btn_mode.Name = "btn_mode";
            this.btn_mode.Size = new System.Drawing.Size(190, 60);
            this.btn_mode.TabIndex = 4;
            this.btn_mode.Text = "  Mode";
            this.btn_mode.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btn_mode.UseVisualStyleBackColor = true;
            this.btn_mode.Click += new System.EventHandler(this.btn_mode_Click);
            // 
            // btn_comm
            // 
            this.btn_comm.FlatAppearance.BorderSize = 0;
            this.btn_comm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_comm.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_comm.ForeColor = System.Drawing.Color.Gainsboro;
            this.btn_comm.Image = ((System.Drawing.Image)(resources.GetObject("btn_comm.Image")));
            this.btn_comm.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_comm.Location = new System.Drawing.Point(10, 135);
            this.btn_comm.Name = "btn_comm";
            this.btn_comm.Size = new System.Drawing.Size(190, 60);
            this.btn_comm.TabIndex = 3;
            this.btn_comm.Text = "  Communication";
            this.btn_comm.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btn_comm.UseVisualStyleBackColor = true;
            this.btn_comm.Click += new System.EventHandler(this.btn_comm_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Maroon;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(200, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(600, 10);
            this.panel2.TabIndex = 1;
            this.panel2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel3_MouseDown);
            this.panel2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel3_MouseMove);
            this.panel2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel3_MouseUp);
            // 
            // serialPort1
            // 
            this.serialPort1.BaudRate = 115200;
            this.serialPort1.PortName = "COM3";
            // 
            // btn_exit
            // 
            this.btn_exit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_exit.BackgroundImage")));
            this.btn_exit.FlatAppearance.BorderSize = 0;
            this.btn_exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_exit.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_exit.ForeColor = System.Drawing.Color.Maroon;
            this.btn_exit.Location = new System.Drawing.Point(752, 10);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(48, 48);
            this.btn_exit.TabIndex = 9;
            this.btn_exit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btn_exit.UseVisualStyleBackColor = true;
            this.btn_exit.Click += new System.EventHandler(this.btn_exit_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Maroon;
            this.panel3.Controls.Add(this.pictureBox1);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(200, 90);
            this.panel3.TabIndex = 14;
            this.panel3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel3_MouseDown);
            this.panel3.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel3_MouseMove);
            this.panel3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel3_MouseUp);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(87, 29);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(47, 50);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Gainsboro;
            this.label1.Location = new System.Drawing.Point(3, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(188, 77);
            this.label1.TabIndex = 6;
            this.label1.Text = "Cl   x";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Maroon;
            this.panel4.Location = new System.Drawing.Point(200, 20);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(350, 10);
            this.panel4.TabIndex = 2;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Maroon;
            this.panel5.Location = new System.Drawing.Point(200, 40);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(250, 10);
            this.panel5.TabIndex = 3;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Maroon;
            this.panel6.Location = new System.Drawing.Point(200, 60);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(150, 10);
            this.panel6.TabIndex = 4;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Gainsboro;
            this.panel9.Location = new System.Drawing.Point(350, 60);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(25, 10);
            this.panel9.TabIndex = 17;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Maroon;
            this.panel7.Location = new System.Drawing.Point(200, 80);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(50, 10);
            this.panel7.TabIndex = 5;
            // 
            // btn_mini
            // 
            this.btn_mini.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_mini.BackgroundImage")));
            this.btn_mini.FlatAppearance.BorderSize = 0;
            this.btn_mini.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_mini.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_mini.ForeColor = System.Drawing.Color.Maroon;
            this.btn_mini.Location = new System.Drawing.Point(704, 10);
            this.btn_mini.Name = "btn_mini";
            this.btn_mini.Size = new System.Drawing.Size(48, 48);
            this.btn_mini.TabIndex = 15;
            this.btn_mini.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btn_mini.UseVisualStyleBackColor = true;
            this.btn_mini.Click += new System.EventHandler(this.btn_mini_Click);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Gainsboro;
            this.panel8.Location = new System.Drawing.Point(250, 80);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(25, 10);
            this.panel8.TabIndex = 16;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Gainsboro;
            this.panel10.Location = new System.Drawing.Point(450, 40);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(25, 10);
            this.panel10.TabIndex = 18;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.Gainsboro;
            this.panel11.Location = new System.Drawing.Point(546, 20);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(25, 10);
            this.panel11.TabIndex = 18;
            // 
            // uctrl_anim1
            // 
            this.uctrl_anim1.BackColor = System.Drawing.Color.LightGray;
            this.uctrl_anim1.ForeColor = System.Drawing.Color.Maroon;
            this.uctrl_anim1.Location = new System.Drawing.Point(200, 100);
            this.uctrl_anim1.Name = "uctrl_anim1";
            this.uctrl_anim1.Size = new System.Drawing.Size(600, 400);
            this.uctrl_anim1.TabIndex = 19;
            // 
            // uctrl_time1
            // 
            this.uctrl_time1.BackColor = System.Drawing.Color.LightGray;
            this.uctrl_time1.ForeColor = System.Drawing.Color.Maroon;
            this.uctrl_time1.Location = new System.Drawing.Point(200, 100);
            this.uctrl_time1.Name = "uctrl_time1";
            this.uctrl_time1.Size = new System.Drawing.Size(600, 400);
            this.uctrl_time1.TabIndex = 13;
            // 
            // uctrl_comm1
            // 
            this.uctrl_comm1.BackColor = System.Drawing.Color.LightGray;
            this.uctrl_comm1.ForeColor = System.Drawing.Color.Maroon;
            this.uctrl_comm1.Location = new System.Drawing.Point(200, 100);
            this.uctrl_comm1.Name = "uctrl_comm1";
            this.uctrl_comm1.Size = new System.Drawing.Size(600, 400);
            this.uctrl_comm1.TabIndex = 10;
            // 
            // uctrl_color1
            // 
            this.uctrl_color1.BackColor = System.Drawing.Color.LightGray;
            this.uctrl_color1.ForeColor = System.Drawing.Color.Maroon;
            this.uctrl_color1.Location = new System.Drawing.Point(200, 100);
            this.uctrl_color1.Name = "uctrl_color1";
            this.uctrl_color1.Size = new System.Drawing.Size(600, 400);
            this.uctrl_color1.TabIndex = 12;
            // 
            // uctrl_mode1
            // 
            this.uctrl_mode1.BackColor = System.Drawing.Color.LightGray;
            this.uctrl_mode1.ForeColor = System.Drawing.Color.Maroon;
            this.uctrl_mode1.Location = new System.Drawing.Point(200, 100);
            this.uctrl_mode1.Name = "uctrl_mode1";
            this.uctrl_mode1.Size = new System.Drawing.Size(600, 400);
            this.uctrl_mode1.TabIndex = 11;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGray;
            this.ClientSize = new System.Drawing.Size(800, 500);
            this.Controls.Add(this.uctrl_anim1);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.btn_mini);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.uctrl_time1);
            this.Controls.Add(this.uctrl_comm1);
            this.Controls.Add(this.uctrl_color1);
            this.Controls.Add(this.uctrl_mode1);
            this.Controls.Add(this.btn_exit);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.ForeColor = System.Drawing.Color.Maroon;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Clox";
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnl_side;
        private System.Windows.Forms.Button btn_time;
        private System.Windows.Forms.Button btn_color;
        private System.Windows.Forms.Button btn_anim;
        private System.Windows.Forms.Button btn_mode;
        private System.Windows.Forms.Button btn_comm;
        private System.Windows.Forms.Panel panel2;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.Button btn_exit;
        private uctrl_comm uctrl_comm1;
        private uctrl_mode uctrl_mode1;
        private uctrl_color uctrl_color1;
        private uctrl_time uctrl_time1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btn_mini;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.PictureBox pictureBox1;
        private uctrl_animation uctrl_anim1;
    }
}

