#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h> 
#include <ESP8266mDNS.h>

ESP8266WiFiMulti wifiMulti;     // Create an instance of the ESP8266WiFiMulti class, called 'wifiMulti'
WiFiServer server(80);  // Create a webserver object that listens for HTTP request on port 80

// server response - HTML - https://html-online.com/editor/const, and added background and text color -|----------------------------------------------|
String s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n<html>\r\n<style>\r\n body{background-color: #d3d3d3; color: #800000;}\r\n</style>\r\n<p>___________________________________________________<br />|<strong>INSTRUCTIONS:</strong><br /> | To send requests to Clox follow the rules:<br /> |&nbsp; - The first letter defines what you want to set: <br /> |&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;- m - mode<br /> |&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;- a - animation<br /> |&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;- c - colors of WS2812 LEDs<br />|&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;- t - time<br /> |&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;- b - brightness of TM1637<br /> |&nbsp; - Modes: - s - stand by - command: ms!<br /> |&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; - c - clock<br /> |&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; - l - lamp<br /> |&nbsp; - Animations: - b - basic animation <br />|&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;- 1 - rotating 1 animation<br />|&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;- 2 - rotating 2 animation<br /> |&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;- z - hide clock numbers<br /> |&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;- f - show four clock numbers<br />|&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;- t - show twelve clock numbers<br />|&nbsp; - Colors: - example: 192.168.1.200/c55.0.0.0.55.0.0.0.55.0.0.0!<br /> |&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |hour&nbsp; |min&nbsp; &nbsp;|sec&nbsp; &nbsp; &nbsp;|back|<br /> |&nbsp; - Time: - example: 192.168.1.200/t14.35.40! -&gt; h.m.s<br /> |&nbsp; - Brightness: - 0-7 - maximum brightness - 7 <br />|&nbsp; - The last symbol: '!' - end of a request<br /> ___________________________________________________</p> </html>\n";
const String start = "Clox.";
String toSTM = start;
void setup(void){
  Serial.begin(115200);         // Start the Serial communication to send messages to the computer
  delay(10);
  
  wifiMulti.addAP("Birca", "11111111");   // add Wi-Fi networks you want to connect to
  wifiMulti.addAP("Antoniyo12", "26022017");
  wifiMulti.addAP("Cloxpot", "11111111"); 
  //Serial.println("Connecting ...");
  int i = 0;
  while (wifiMulti.run() != WL_CONNECTED) { // Wait for the Wi-Fi to connect: scan for Wi-Fi networks, and connect to the strongest of the networks above
    delay(250);
    //Serial.print('.');
  }
  //Serial.println('\n');
  //Serial.print("Connected to ");
  //Serial.println(WiFi.SSID());  
  //Serial.print("IP address:\t");
  //Serial.println(WiFi.localIP());
  Serial.print(start);                    //sending data about connection to stm32
  Serial.print("w");                      
  Serial.print(WiFi.SSID());
  Serial.print(".");
  Serial.print(WiFi.localIP());
  Serial.print("!");
  server.begin();                           // Actually start the server
  //Serial.println("HTTP server started");
  
}

void loop(void){
  WiFiClient client = server.available();// Check if a client has connected
  if (!client) return;
  
  while (!client.available()) delay(1);// Wait until the client sends some data

  
  String req = client.readStringUntil('\r');// Read the first line of the request

  // Process the request
  if (req.indexOf("/") != -1) {
    //Get only important data out of request and send it to STM
    int i,j;
    for(i = 5;req[i]!=' ';i++){
      toSTM+=req[i];
      if(req[i]=='!'&&req[i+1]!=' ')
        toSTM+=start;
    } 
    if(toSTM!=start&&toSTM!=start+"favicon.ico")
      Serial.print(toSTM);
    toSTM=start;
  }

  client.flush();

  // Send the response to the client
  client.print(s);
  delay(1); 
}
