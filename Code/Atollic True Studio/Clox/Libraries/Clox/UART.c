#include "UART.h"
/*
 * Add this to the main.c to handle interrupts
	char c;
	void USART1_IRQHandler(void)
	{
		c = USART_ReceiveData(USART1);
//
//		  YOUR CODE...
//
	USART_ClearITPendingBit(USART1, USART_IT_RXNE);
	}
*/
void UART_Init(void)
{
	//USART1 -> USB
	//USART2 -> ESP
	GPIO_InitTypeDef GPIO_InitStruct;
	USART_InitTypeDef USART_InitStruct;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_USART1, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2,ENABLE);

	//2a - Inicijalizacija Tx pina (GPIOA, GPIO_Pin_9)
    GPIO_StructInit(&GPIO_InitStruct);
    GPIO_InitStruct.GPIO_Pin = TX1;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStruct);

	//2b - Inicijalizacija Rx pina (GPIOA, GPIO_Pin_10) i za USART2 (GPIOA, GPIO_Pin_3)
	GPIO_InitStruct.GPIO_Pin = RX1|RX2;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStruct);

	//2c - komunikacijski parametri USART1
	USART_StructInit(&USART_InitStruct);
	USART_InitStruct.USART_BaudRate = 115200;
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;
	USART_InitStruct.USART_Mode =  USART_Mode_Rx|USART_Mode_Tx;
	USART_InitStruct.USART_Parity = USART_Parity_No;
	USART_InitStruct.USART_StopBits = USART_StopBits_1;
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_Init(USART1, &USART_InitStruct);
	USART_Cmd(USART1, ENABLE);
	USART_ITConfig(USART1,USART_IT_RXNE,ENABLE); //omoguci USART prekid uslijed primanja podataka
	//podesi priority grouping: 4 bita za preemptive priority, 0 bita za subpriority
	NVIC_SetPriorityGrouping(NVIC_PriorityGroup_4);
	//postavi prioritet USART1_IRQn prekida
	NVIC_SetPriority(USART1_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0,0));
	//omoguci USART1_IRQn prekid
	NVIC_EnableIRQ(USART1_IRQn);

	//sve isto za USART2
	USART_Init(USART2, &USART_InitStruct);
	USART_Cmd(USART2, ENABLE);
	USART_ITConfig(USART2,USART_IT_RXNE,ENABLE);
	NVIC_SetPriorityGrouping(NVIC_PriorityGroup_4);
	NVIC_SetPriority(USART2_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0,0));
	NVIC_EnableIRQ(USART2_IRQn);
}


