#include "TM1637.h"

const char segmentMap[] =
{
    0x3f, 0x06, 0x5b, 0x4f, 0x66, 0x6d, 0x7d, 0x07, // 0-7
    0x7f, 0x6f, 0x77, 0x7c, 0x39, 0x5e, 0x79, 0x71, // 8-9, A-F
    0x00                                            // blank
};

void TM1637Init(void)
{
    RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA, ENABLE);
    GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Pin = CLK | DIO;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
    //set the brightness
    TM1637SetBrightness(3);
    TM1637Blank();
}

void TM1637Start(void)
{
    TM1637ClkHigh();
    TM1637DatHigh();
    delay_usec(2);
    TM1637DatLow();
}

void TM1637Stop(void)
{
    TM1637ClkLow();
    delay_usec(2);
    TM1637DatLow();
    delay_usec(2);
    TM1637ClkHigh();
    delay_usec(2);
    TM1637DatHigh();
}

void TM1637ReadAck(void)
{
    TM1637ClkLow();
    delay_usec(5);
    TM1637ClkHigh();
    delay_usec(2);
    TM1637ClkLow();
}

void TM1637WriteByte(unsigned char b)
{
    unsigned char i;
    for ( i = 0; i < 8; ++i)
    {
        TM1637ClkLow();
        if(b & 0x01){ TM1637DatHigh(); }
        else{ TM1637DatLow(); }
        delay_usec(3);
        b >>= 1;
        TM1637ClkHigh();
        delay_usec(3);
    }
}

void TM1637SetBrightness(unsigned char brightness)
{
    // Brightness command:
    // 1000 0XXX = display off
    // 1000 1BBB = display on, brightness 0-7
    // X = don't care
    // B = brightness
    TM1637Start();
    TM1637WriteByte(0x87 + brightness);
    TM1637ReadAck();
    TM1637Stop();
}

void TM1637Segments(unsigned int digitArr[])
{
	int i;
	TM1637Start();
	TM1637WriteByte(0x40);
	TM1637ReadAck();
	TM1637Stop();

	TM1637Start();
	TM1637WriteByte(0xc0);
	TM1637ReadAck();

	for (i = 0; i < 4; ++i)
	{
		TM1637WriteByte(digitArr[3 - i]);
		TM1637ReadAck();
	}
	TM1637Stop();
}

void TM1637Number(unsigned int v)
{
    unsigned int digitArr[4]={segmentMap[16]};
    int i;
    for (i = 0; i < 4; ++i)
    {
        digitArr[i] = segmentMap[v % 10];
        v /= 10;
    }
    for (i = 3; i > 0; --i)
    	if(digitArr[i] == segmentMap[0])
    		digitArr[i] = segmentMap[16];
    	else
    		break;
    TM1637Segments(digitArr);
}

void TM1637Time(unsigned int h, unsigned int m, unsigned int s)
{
    unsigned int digitArr[4];
    digitArr[0] = segmentMap[m % 10];
    digitArr[1] = segmentMap[m / 10];
    digitArr[2] = segmentMap[h % 10];
    if(s%2)
        digitArr[2] |= 1 << 7;
    if(h/10==0)
    	digitArr[3] = segmentMap[16];
    else
        digitArr[3] = segmentMap[h / 10];
    TM1637Segments(digitArr);
}

void TM1637Blank()
{
    unsigned int digitArr[4];
    unsigned char i;
    for (i = 0; i < 4; ++i)
		digitArr[i] = segmentMap[16];
    TM1637Segments(digitArr);
}
