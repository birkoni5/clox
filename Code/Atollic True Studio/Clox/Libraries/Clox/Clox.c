/*
******************************************************************************
Project: Clox
Author: Toni Birka, toni_birka@yahoo.com
******************************************************************************
* Pins on STM
* PB6  -> DIN WS2812
* PA3  -> TX ESP8266-01
* PA10 -> TX USB-TTL Converter
* PA9  -> RX USB-TTL Converter
* PA0  -> CLK TM1637
* PA1  -> DIO TM1637
******************************************************************************
REQUEST INSTRUCTIONS:
 * To start writing command: Clox.
 * The first letter: - m - mode
 * 					 - a - animation
 * 				 	 - c - colors of ws
 * 					 - t - time
 * 					 - b - brightness of TM1637
 * mode: - s - stand by - command: Clox.ms!
 * 		 - c - clock
 * 		 - l - lamp
 * animation: - b - basic animation
 * 			  - 1 - rotating 1 animation
 * 			  - 2 - rotating 2 animation
 * 			  - z - hide clock numbers
 * 			  - f - show four clock numbers
 * 			  - t - show twelve clock numbers
 * colors: - example: Clox.c55.0.0.0.55.0.0.0.55.0.0.0! //max length 12x3+11=47
 * 					  	   |hour  |minute|second|back |
 * example of setting mode and colors: Clox.mc!Clox.c55.0.0.0.55.0.0.0.55.55.55.55!
 * time: - example: Clox.t14.35.40! - h.m.s
 * brightness of TM1637: - value from 0 to 7, 7 is max brightness
 * The last symbol: - ! - end of request
 */
#include <stddef.h>
#include "Clox.h"

const RGB red = {55,0,0};
const RGB green = {0,55,0};
const RGB blue = {0,0,55};
const RGB white = {55,55,55};
const RGB black = {0,0,0};
const Time zero = {0,0,0};
char mode;
Clox_Color colors;
Time time;
RGB colors_of_LEDs[NUM_LED]; // used for clock mode
const uint16_t timerValue = 4; //it is recommended to be a divisor of 1000, min value - 4
uint16_t ms;
void (*animation)(void);
uint8_t led, cNums4,cNums12;
uint16_t* speed;
uint16_t speed0 = 1000;
uint16_t speed1[60]={  // sum = 1000ms - 1s
		20,20,20,20,20,20,20,20,20,20,
		16,16,16,16,16,16,16,16,16,16,
		16,16,16,16,16,16,16,16,16,16,
		16,16,16,16,16,16,16,16,16,16,
		16,16,16,16,16,16,16,16,16,16,
		16,16,16,16,16,16,16,16,16,16

};
uint16_t speed2[60]={  // sum = 1000ms - 1s
		200,
		16,16,16,16,16,16,16,16,16,16,
		16,16,16,16,16,16,16,16,16,16,
		16,16,16,
		12,12,12,12,12,12,12,12,12,12,
		12,12,12,12,12,12,12,12,12,12,
		12,12,12,12,12,12,12,12,12,12,
		12,12,12,12,12,12

};
// ESP data about connection
char SSID_and_IP[50];
//UART variables
const char start[]="Clox.";
char req[REQ_LEN];
char cmd;
int8_t el, pre;
int cs[12];
void UART_data(char x)
{
	if(pre<5){
		if(start[pre]==x)
			pre++;
		else if(start[0]==x)//Receiving command after noise
			pre=1;
		else
			pre=0;
	}else if(pre==5){
			pre++;
			cmd=x;
	}else if(pre==6&&x!='!')
		req[el++]=x;
	else if(x=='!'){
		switch(cmd){
			   case 'm' :
				   if(strlen(req)==1)
					   Clox_SetMode(req[0]);
				   break;
			   case 'a' :
				   if(strlen(req)==1)
					   Clox_SetAnimation(req[0]);
				   break;
			   case 'c' :
				   if(strlen(req)>=23&&strlen(req)<=47){
					   //serialWriteLine(req);
					   strToInt(req,cs);
					   Clox_SetColors(intArrToRGB(cs),intArrToRGB(&cs[3]),intArrToRGB(&cs[6]),intArrToRGB(&cs[9]));
				   }
				   break;
			   case 't' :
				   if(strlen(req)>=5&&strlen(req)<=8){
					   strToInt(req,cs);
					   time.h = cs[0];
					   time.m = cs[1];
					   time.s = cs[2];
				   }
				   break;
			   case 'b' :
				   if(strlen(req)==1)
					   TM1637SetBrightness(req[0]-'0');
				   break;
			   case 'w' :
				   if(strlen(req)>0)
					   strcpy(SSID_and_IP,req);
				   else if(strlen(SSID_and_IP)==0)
					   serialWriteLine("."); //ESP is not connected
				   else
					   serialWriteLine(SSID_and_IP);
				   break;
			   default  :
				   break;
			   }
		for(int i = 0;i<REQ_LEN;i++)
			req[i]=0;
		el=0;
		pre=0;
	}
}

void USART1_IRQHandler(void)//data from computer
{
	char x = USART_ReceiveData(USART1);
	UART_data(x);
USART_ClearITPendingBit(USART1, USART_IT_RXNE);
}

void USART2_IRQHandler(void)// data from ESP
{
	char x = USART_ReceiveData(USART2);
	UART_data(x);
USART_ClearITPendingBit(USART2, USART_IT_RXNE);
}

void serialWriteLine(char* str) //for debugging
{
	for(int i=0;i<strlen(str);i++){
		while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
		USART_SendData(USART1,str[i]);
	}
	while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
	USART_SendData(USART1,'\r');
	while(USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
	USART_SendData(USART1,'\n');
}

void strToInt(char* str, int* x)
{
	char* tmp;
	uint8_t i = 0;
	tmp = strtok (str," ,.-");
	i=0;
	while (tmp != NULL){
		x[i++]=strtol(tmp,NULL,10);
		tmp = strtok (NULL, " ,.-");
	}
}

void Clox_Init(void)
{
	UART_Init();
	TM1637Init();
	delay_Init(0);
	ws2812_Init();
	time = zero;
	Clox_SetColors(red, green, blue, black);
	Clox_SetAnimation('b');
	Clox_SetMode('s');
}

void Clox_Loop(void)
{
	while(1){
		if(timer_Get()==0){ //when 4(timerValue) ms passed
			timer_Set(timerValue); //minimum value which can be set here
			ms+=timerValue;
			if(mode =='c'&&ms%speed[led]==0){
				(*animation)();
				led++;
			}
			if(ms>=1000){
				Clox_IncreaseTime();
				ms = 0;
				led = 0;
				if(mode=='s') //Standby
					Clox_ToggleLEDOnTop();//Toggle LED on the top
				else if(mode=='c')
					TM1637Time(time.h,time.m,time.s);
			}
		}
	}
}

void Clox_ToggleLEDOnTop(void){
	cmp_RGB(ws2812_Get_Color(SHIFT),white)?
			ws2812_Set_Led(SHIFT,black):
			ws2812_Set_Led(SHIFT,white);
}

void Clox_IncreaseTime()
{
	time.s++;
	if(time.s==60){
		time.s=0;
		time.m++;
		if(time.m==60){
			time.m=0;
			time.h++;
			if(time.h==24)
				time.h=0;
		}
	}
}

void Clox_SetColors(RGB h, RGB m, RGB s, RGB b)
{
	colors.h = h;
	colors.m = m;
	colors.s = s;
	colors.back = b;
	if(mode == 'l')
		Clox_SetMode('l'); //Update color of lamp
}

void Clox_SetMode(char m)
{
	switch(m){
	   case 's' :
		   mode = 's';
		   ws2812_Set_All(black);
		   TM1637Blank();
		   break;
	   case 'c' :
		   mode = 'c';
		   ws2812_Set_All(colors.back);
		   break;
	   case 'l' :
		   mode = 'l';
		   if(cmp_RGB(colors.back,black))
			   ws2812_Set_All(white);
		   else
			   ws2812_Set_All(colors.back);
		   TM1637Blank();
		   break;
	   default  :
		   break;
	   }
}

char Clox_GetMode(void)
{
	return mode;
}

void Clox_SetAnimation(char a)
{
	switch(a){
	   case 'b' :
		   animation=&Clox_BasicAnimation;
		   speed=&speed0;
		   break;
	   case '1' :
		   animation=&Clox_RotatingA1;
		   speed=speed1;
		   break;
	   case '2' :
		   animation=&Clox_RotatingA2;
		   speed=speed2;
		   break;
	   case 'f' :
		   cNums4=1;
		   cNums12=0;
		   break;
	   case 't' :
		   cNums12=1;
		   cNums4=0;
		   break;
	   case 'z' :
		   cNums12=0;
		   cNums4=0;
		   break;
	   default  :
		   break;
	   }
}

void Clox_BasicAnimation(void)
{
	Clox_AInCommon();
	colors_of_LEDs[(time.s+SHIFT)%NUM_LED]=colors.s;
	ws2812_Set_Aray(colors_of_LEDs);
}

void Clox_RotatingA1(void)
{
	Clox_AInCommon();
	colors_of_LEDs[(time.s+SHIFT)%NUM_LED]=colors.s;
	colors_of_LEDs[(led+SHIFT)%NUM_LED]=colors.s;
	ws2812_Set_Aray(colors_of_LEDs);
}

void Clox_RotatingA2(void)
{
	Clox_AInCommon();
	colors_of_LEDs[(led+time.s+SHIFT)%NUM_LED]=colors.s;
	ws2812_Set_Aray(colors_of_LEDs);
}

void Clox_AInCommon(void)
{
	for(int i = 0; i < NUM_LED; i++)
		colors_of_LEDs[i]=colors.back;
	for(int i = 0; cNums4 && i < 4; i++)
		colors_of_LEDs[(15*i+SHIFT)%NUM_LED]=white;
	for(int i = 0; cNums12 && i < 12; i++)
		colors_of_LEDs[(5*i+SHIFT)%NUM_LED]=white;
	for(int i = 0; i < 3; i++)
		colors_of_LEDs[((time.h)*5+i+SHIFT-1)%NUM_LED]=colors.h;
	colors_of_LEDs[(time.m+SHIFT)%NUM_LED]=colors.m;
}
