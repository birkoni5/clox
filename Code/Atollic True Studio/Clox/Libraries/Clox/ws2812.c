/*
 *  Project:	Lightwight WS2812 STM32 Lib
 *  Author:	DraconiX
 *
 *  File:	ws2812.c
 *  Version:	V1.0
 *  Header:	StdPeripherial (STM)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stddef.h>
#include <stm32f10x.h>
#include <stm32f10x_dma.h>
#include <stm32f10x_tim.h>
#include <stm32f10x_rcc.h>
#include <stm32f10x_gpio.h>
#include <stm32f10x_usart.h>
#include "ws2812.h"


/* WS2812 Init function
 *
 * value:	Nothing
 * return:	Nothing
 */
void ws2812_Init() {
	//Enabling Clocks
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

	//Setting up the Pin (PB6) for PWM use - the Pin for the DIN at the WS2812
	GPIO_InitTypeDef pwm_out;
	pwm_out.GPIO_Pin = GPIO_Pin_6;
	pwm_out.GPIO_Speed = GPIO_Speed_50MHz;
	pwm_out.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOB, &pwm_out);

	//Setting up the Timer, we use Timer4.
	TIM_TimeBaseInitTypeDef tim4;
	tim4.TIM_Prescaler = 8;
	tim4.TIM_Period = 9;
	tim4.TIM_ClockDivision = TIM_CKD_DIV1;
	tim4.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM4, &tim4);

	// Init PWM & Output Compare Mode
	TIM_OCInitTypeDef pwm;
	pwm.TIM_OCMode = TIM_OCMode_PWM1;
	pwm.TIM_OutputState = TIM_OutputState_Enable;
	pwm.TIM_Pulse = 0;
	pwm.TIM_OCPolarity = TIM_OCPolarity_High;

	TIM_OCStructInit(&pwm);
	TIM_OC1PreloadConfig(TIM4, 8);
	pwm.TIM_OCMode = TIM_OCMode_PWM1;
	pwm.TIM_OutputState = TIM_OutputState_Enable;

	TIM_OC1Init(TIM4, &pwm);

	// Init the DMA Channel Memory -> Timer4.CompareRegister
	DMA_InitTypeDef dma;
	dma.DMA_PeripheralBaseAddr = (uint32_t)&TIM4->CCR1;
	dma.DMA_MemoryBaseAddr = (uint32_t)(&ws2812_buffer[0]);
	dma.DMA_DIR = DMA_DIR_PeripheralDST;
	dma.DMA_BufferSize = BUFFER_SIZE;
	dma.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	dma.DMA_MemoryInc = DMA_MemoryInc_Enable;
	dma.DMA_PeripheralDataSize =  DMA_PeripheralDataSize_HalfWord;
	dma.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	dma.DMA_Mode = DMA_Mode_Circular;
	dma.DMA_Priority = DMA_Priority_High;
	dma.DMA_M2M = DMA_M2M_Disable;

	DMA_Init(DMA1_Channel1, &dma);
	TIM_DMACmd(TIM4, TIM_DMA_CC1, ENABLE);
	
	DMA_Cmd(DMA1_Channel1, ENABLE);
	TIM_Cmd(TIM4, ENABLE);


#ifdef FREERUNNING
	// Set to Freeruning Mode (see ws2812.h)
	DMA_Cmd(DMA1_Channel1, ENABLE);
	TIM_Cmd(TIM4, ENABLE);
#endif
	RGB black = {0,0,0};
	ws2812_Set_All( black);
#ifndef FREERUNNING
	ws2812_Refresh();
#endif
}

/* WS2812 Refresh function
 * it you dont use the freeruning Mode,
 * you MUST refresh the WS2812 manualy.
 *
 * value:	Nothing
 * return:	Nothing
 */
void ws2812_Refresh()
{
	DMA_Cmd(DMA1_Channel1, ENABLE);
	TIM_Cmd(TIM4, ENABLE);
	while(!DMA_GetFlagStatus(DMA1_FLAG_TC1));
	TIM_Cmd(TIM4, DISABLE);
	DMA_Cmd(DMA1_Channel1, DISABLE);
	DMA_ClearFlag(DMA1_FLAG_TC1);
}

/* Get LED Color function
 *
 * value:	LedPos - the LED Position in the String
 * return:	RGB Color Values - Range 0-255
 */
RGB ws2812_Get_Color(uint16_t LedPos)
{
	RGB Color = {0,0,0};
	uint16_t pos = LedPos*24;

	if(ws2812_buffer[pos+0] == CMPH) Color.g |= (1<<7);
	if(ws2812_buffer[pos+1] == CMPH) Color.g |= (1<<6);
	if(ws2812_buffer[pos+2] == CMPH) Color.g |= (1<<5);
	if(ws2812_buffer[pos+3] == CMPH) Color.g |= (1<<4);
	if(ws2812_buffer[pos+4] == CMPH) Color.g |= (1<<3);
	if(ws2812_buffer[pos+5] == CMPH) Color.g |= (1<<2);
	if(ws2812_buffer[pos+6] == CMPH) Color.g |= (1<<1);
	if(ws2812_buffer[pos+7] == CMPH) Color.g |= (1<<0);

	pos += 8;

	if(ws2812_buffer[pos+0] == CMPH) Color.r |= (1<<7);
	if(ws2812_buffer[pos+1] == CMPH) Color.r |= (1<<6);
	if(ws2812_buffer[pos+2] == CMPH) Color.r |= (1<<5);
	if(ws2812_buffer[pos+3] == CMPH) Color.r |= (1<<4);
	if(ws2812_buffer[pos+4] == CMPH) Color.r |= (1<<3);
	if(ws2812_buffer[pos+5] == CMPH) Color.r |= (1<<2);
	if(ws2812_buffer[pos+6] == CMPH) Color.r |= (1<<1);
	if(ws2812_buffer[pos+7] == CMPH) Color.r |= (1<<0);

	pos += 8;

	if(ws2812_buffer[pos+0] == CMPH) Color.b |= (1<<7);
	if(ws2812_buffer[pos+1] == CMPH) Color.b |= (1<<6);
	if(ws2812_buffer[pos+2] == CMPH) Color.b |= (1<<5);
	if(ws2812_buffer[pos+3] == CMPH) Color.b |= (1<<4);
	if(ws2812_buffer[pos+4] == CMPH) Color.b |= (1<<3);
	if(ws2812_buffer[pos+5] == CMPH) Color.b |= (1<<2);
	if(ws2812_buffer[pos+6] == CMPH) Color.b |= (1<<1);
	if(ws2812_buffer[pos+7] == CMPH) Color.b |= (1<<0);

	return Color;
}
/* Set Led to Color
 *
 * value:	LedPos 	- the LED Position in the String
 * 			RGB	- the the values of red, blue, green
 * return:	Nothing
 */
void ws2812_Set_Led(uint32_t LedPos, RGB Color)
{
	// Calc Array Position
	uint32_t PosG = LedPos*24;
	uint32_t PosR = PosG+8;
	uint32_t PosB = PosG+16;

	// Setting up the Green Color (the WS281 is not RGB, it is GRB)
	ws2812_buffer[PosG+0] = (Color.g & 0x80) ? CMPH:CMPL;
	ws2812_buffer[PosG+1] = (Color.g  & 0x40) ? CMPH:CMPL;
	ws2812_buffer[PosG+2] = (Color.g  & 0x20) ? CMPH:CMPL;
	ws2812_buffer[PosG+3] = (Color.g  & 0x10) ? CMPH:CMPL;
	ws2812_buffer[PosG+4] = (Color.g  & 0x08) ? CMPH:CMPL;
	ws2812_buffer[PosG+5] = (Color.g  & 0x04) ? CMPH:CMPL;
	ws2812_buffer[PosG+6] = (Color.g  & 0x02) ? CMPH:CMPL;
	ws2812_buffer[PosG+7] = (Color.g  & 0x01) ? CMPH:CMPL;

	// Setting up the Red Color (the WS281 is not RGB, it is GRB)
	ws2812_buffer[PosR+0] = (Color.r  & 0x80) ? CMPH:CMPL;
	ws2812_buffer[PosR+1] = (Color.r & 0x40) ? CMPH:CMPL;
	ws2812_buffer[PosR+2] = (Color.r & 0x20) ? CMPH:CMPL;
	ws2812_buffer[PosR+3] = (Color.r & 0x10) ? CMPH:CMPL;
	ws2812_buffer[PosR+4] = (Color.r & 0x08) ? CMPH:CMPL;
	ws2812_buffer[PosR+5] = (Color.r & 0x04) ? CMPH:CMPL;
	ws2812_buffer[PosR+6] = (Color.r & 0x02) ? CMPH:CMPL;
	ws2812_buffer[PosR+7] = (Color.r & 0x01) ? CMPH:CMPL;

	// Setting up the Blue Color (the WS281 is not RGB, it is GRB)
	ws2812_buffer[PosB+0] = (Color.b & 0x80) ? CMPH:CMPL;
	ws2812_buffer[PosB+1] = (Color.b & 0x40) ? CMPH:CMPL;
	ws2812_buffer[PosB+2] = (Color.b & 0x20) ? CMPH:CMPL;
	ws2812_buffer[PosB+3] = (Color.b & 0x10) ? CMPH:CMPL;
	ws2812_buffer[PosB+4] = (Color.b & 0x08) ? CMPH:CMPL;
	ws2812_buffer[PosB+5] = (Color.b & 0x04) ? CMPH:CMPL;
	ws2812_buffer[PosB+6] = (Color.b & 0x02) ? CMPH:CMPL;
	ws2812_buffer[PosB+7] = (Color.b & 0x01) ? CMPH:CMPL;
}
/* Set Led to Color
 *
 * value:	RGB	- the the values of red, blue, green
 * return:	Nothing
 */
void ws2812_Set_All(RGB Color)
{
	for(int i = 0;i<NUM_LED;i++)
		ws2812_Set_Led(i,Color);
}
/* Compare two colors
 *
 * value:	2 RGB colors
 * return:	1 if true, else 0
 */
uint8_t cmp_RGB(RGB C1, RGB C2)
{
	return (C1.r==C2.r&&C1.g==C2.g&&C1.b==C2.b)? 1:0;
}
/* Convert integer array to RGB
 *
 * value:	array of int colors
 * return:	RGB
 */
RGB intArrToRGB(int* c)
{
	RGB rgb = {c[0],c[1],c[2]};
	return rgb;
}
/* Multiply RGB by a fraction
 *
 * value:	numerator, denominator, and RGB
 * return:	RGB
 */
RGB multiplyByFrac(int num,int den, RGB c)
{
	RGB rgb = {num*c.r/den,num*c.g/den,num*c.b/den};
	return rgb;
}
/* Set stip zo colors -> FREERUNNING mode
 *
 * value:	RGB array of colors
 * return:	Nothing
 */
void    ws2812_Set_Aray(RGB* Color){
	for(int i = 0; i<60;i++)
		ws2812_Set_Led(i, Color[i]);
}
