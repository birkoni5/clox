#include "stm32f10x.h"
#include "delay.h"

#define CLK   GPIO_Pin_0
#define DIO   GPIO_Pin_1

#define TM1637ClkHigh(); GPIO_SetBits(GPIOA, CLK);
#define TM1637ClkLow();  GPIO_ResetBits(GPIOA, CLK);
#define TM1637DatHigh(); GPIO_SetBits(GPIOA, DIO);
#define TM1637DatLow();  GPIO_ResetBits(GPIOA, DIO);

void TM1637Init				(void);
void TM1637Start			(void);
void TM1637Stop				(void);
void TM1637ReadAck			(void);
void TM1637WriteByte		(unsigned char b);
void TM1637SetBrightness	(unsigned char brightness);
void TM1637Segments			(unsigned int digitArr[]);
void TM1637Number			(unsigned int v);
void TM1637Time				(unsigned int h,unsigned int m,unsigned int s);
void TM1637Blank			(void);
