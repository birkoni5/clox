/*
******************************************************************************
Project: Clox
Author: Toni Birka, toni_birka@yahoo.com
******************************************************************************
* Pins on STM
* PB6  -> DIN WS2812
* PA10 -> TX ESP8266-01
* PA0  -> CLK TM1637
* PA1  -> DIO TM1637
******************************************************************************
*/
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <ws2812.h>
#include "delay.h"
#include <stm32f10x.h>
#include "TM1637.h"
#include "UART.h"

#define SHIFT 29 //depends on construction of Clox, to get 12 o'clock on the top
				 // 1 - 60
#define REQ_LEN 50 //Length of request array
/*---------------------------------------------------------------------------------------------------------------------------------------------------
 * Clox structure definition
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 */
typedef struct
{
  RGB h,m,s,back;
} Clox_Color;

typedef struct
{
  uint8_t h,m,s;
} Time;
/*---------------------------------------------------------------------------------------------------------------------------------------------------
 * Clox interface definition
 *---------------------------------------------------------------------------------------------------------------------------------------------------
 */
void Clox_Init				(void);
void Clox_Loop				(void);
void Clox_ToggleLEDOnTop	(void);
void Clox_IncreaseTime		(void);
void Clox_SetColors			(RGB h, RGB m, RGB s, RGB b);
char Clox_GetMode			(void);
void Clox_SetMode			(char m);
void strToInt				(char* str, int* x);
void Clox_SetAnimation		(char a);
void Clox_BasicAnimation	(void);
void Clox_RotatingA1		(void);
void Clox_RotatingA2		(void);
void Clox_AInCommon			(void);
void UART_data			    (char x);
void serialWriteLine		(char* str);
